package com.chanchhaya.note.ams.view.article;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.chanchhaya.note.ams.adapter.article.ListArticleAdapter;
import com.chanchhaya.note.ams.api.request.CreateArticleRequest;
import com.chanchhaya.note.ams.api.response.BaseResponse;
import com.chanchhaya.note.ams.databinding.ActivityActiclesListBinding;
import com.chanchhaya.note.ams.model.Article;
import com.chanchhaya.note.ams.model.Pagination;
import com.chanchhaya.note.ams.utils.PaginationUtil;
import com.chanchhaya.note.ams.viewmodel.ArticleViewModel;

import java.util.ArrayList;
import java.util.List;

public class ListArticlesActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener, ListArticleAdapter.OnActionClick {

    private ActivityActiclesListBinding binding;
    private ListArticleAdapter listArticleAdapter;
    private LinearLayoutManager linearLayoutManager;

    private boolean isReload = false;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    private boolean isCreate = false;
    private boolean isUpdate = false;
    private int currentItemUpdate = 0;

    private Pagination pagination;

    private ArticleViewModel articleViewModel;

    /**
     * Set up common task in onCreate() callback
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityActiclesListBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        configRecyclerView();

        binding.swipe.setOnRefreshListener(this);
        onFabClick();

        articleViewModel = new ViewModelProvider(this).get(ArticleViewModel.class);
        articleViewModel.init();

        getArticleLiveData();

    }

    /**
     * onStart() callback is used to
     * - Refresh data when inserted new article successfully
     * - Update data when updated existing article successfully by using notifyItemChanged(position)
     */
    @Override
    protected void onStart() {
        super.onStart();
        if (isCreate) {
            onRefresh();
        }
        if (isUpdate) {
            Log.d("TAG", "onStart: update");
            listArticleAdapter.getDataSet().get(currentItemUpdate).setTitle("New Title");
            listArticleAdapter.notifyItemChanged(currentItemUpdate);
            isUpdate = false;
        }
    }

    /**
     * This method is used to set the event of Floating Action Button
     */
    private void onFabClick() {
        binding.fabCreateNew.setOnClickListener(v -> {
            isCreate = true;
            Intent intent = new Intent(this, CreateArticleActivity.class);
            startActivity(intent);
        });
    }

    /**
     * this method is used to config the setting of recycler view
     */
    private void configRecyclerView() {

        pagination = new Pagination();

        linearLayoutManager = new LinearLayoutManager(this);
        listArticleAdapter = new ListArticleAdapter(this, new ArrayList<>());
        binding.rcvListArticle.setLayoutManager(linearLayoutManager);
        binding.rcvListArticle.setAdapter(listArticleAdapter);

        binding.rcvListArticle.addOnScrollListener(new PaginationUtil(linearLayoutManager, pagination.getLimit()) {

            @Override
            protected void loadMoreItems() {
                isLoading = true;
                pagination.increasePage();
                getArticleLiveData();
            }

            @Override
            public boolean isLastPage() {
                return pagination.getPage() == pagination.getTotalPages();
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });

    }

    /**
     * This method is used to get article live data which subscribe on view model
     */
    private void getArticleLiveData() {

        // show loading
        if (isLoading) {
            binding.progressBar.setVisibility(View.VISIBLE);
        }

        // view subscribes live data
        articleViewModel.getArticleLiveData(pagination).observe(this, listBaseResponse -> {
            // after get article live data is success
            binding.progressBar.setVisibility(View.GONE);
            // set swipe loading to invisible
            binding.swipe.setRefreshing(false);
            // modify data set
            listArticleAdapter.setDataSet(listBaseResponse.getData());

            // set behavior to default
            isReload = false;
            isLoading = false;
        });

    }

    /**
     * មុខងានេះប្រើដើម្បីធ្វើការទាញយកអត្ថបទថ្មីៗសារឡើងវិញ
     */
    @Override
    public void onRefresh() {
        // clear data set in adapter
        listArticleAdapter.getDataSet().clear();
        // set reload equals true
        isReload = true;
        // set create equals false
        isCreate = false;
        // You must set page = 1 when you are going to refresh
        pagination.setPage(1);
        // ហៅមុខងារដើម្បីចាប់យកទិន្នន័យឡើងវិញ
        getArticleLiveData();
    }

    /**
     * Click on recycler view item to view detail information about article
     * @param position is index of item on recycler view
     */
    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, DetailArticleActivity.class);
        intent.putExtra("article", listArticleAdapter.getDataSet().get(position));
        startActivity(intent);
    }

    /**
     * Click on edit button is edit the article in CreateArticleActivity.class
     * @param position is index of item on recycler view
     */
    @Override
    public void onEdit(int position) {

        isUpdate = true;

        currentItemUpdate = position;
        Article articleDataSet = listArticleAdapter.getDataSet().get(position);
        CreateArticleRequest article = new CreateArticleRequest();

        int id = articleDataSet.getId();
        article.setTitle(articleDataSet.getTitle());
        article.setDescription(articleDataSet.getDescription());
        article.setImage(articleDataSet.getImageUrl());

        Intent intent = new Intent(this, CreateArticleActivity.class);
        intent.putExtra("article", article);
        intent.putExtra("id", id);
        startActivity(intent);

    }

    /**
     * Click on delete button is delete the article by id when confirm in message dialog
     * @param position is index of item on recycler view
     */
    @Override
    public void onDelete(int position) {
        new AlertDialog.Builder(this)
                .setTitle("Delete operation!")
                .setPositiveButton("Delete!", (dialog, which) -> {
                    int id = listArticleAdapter.getDataSet().get(position).getId();
                    articleViewModel.deleteById(id).observe(this, articleBaseResponse -> {
                        Toast.makeText(ListArticlesActivity.this, articleBaseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        listArticleAdapter.notifyItemRemoved(position);
                    });
                })
                .setCancelable(true)
                .show();
    }

}