package com.chanchhaya.note.ams.view.article;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.chanchhaya.note.ams.R;
import com.chanchhaya.note.ams.databinding.ActivityDetailArticleBinding;
import com.chanchhaya.note.ams.model.Article;

public class DetailArticleActivity extends AppCompatActivity {

    private ActivityDetailArticleBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_article);
        binding = ActivityDetailArticleBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        // invoke new intent
        onNewIntent(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Article article = (Article) intent.getSerializableExtra("article");
        if (article != null) {
            setDetailViews(article);
        }
    }

    private void setDetailViews(Article article) {
        Glide.with(this)
                .load(article.getImageUrl())
                .into(binding.imageArticle);
        binding.textTitle.setText(article.getTitle());
        binding.textDescription.setText(article.getDescription());
    }

}