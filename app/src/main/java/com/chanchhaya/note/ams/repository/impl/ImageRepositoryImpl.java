package com.chanchhaya.note.ams.repository.impl;

import android.net.Uri;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;

import com.chanchhaya.note.ams.api.ImageService;
import com.chanchhaya.note.ams.api.config.RetrofitConfig;
import com.chanchhaya.note.ams.api.response.ImageUploadResponse;
import com.chanchhaya.note.ams.repository.ImageRepository;
import com.chanchhaya.note.ams.utils.FileUtils;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageRepositoryImpl implements ImageRepository {

    private final ImageService imageService;

    public ImageRepositoryImpl() {
        imageService = RetrofitConfig.createService(ImageService.class);
    }

    @Override
    public MutableLiveData<ImageUploadResponse> uploadSingle(MultipartBody.Part file) {

        MutableLiveData<ImageUploadResponse> liveData = new MutableLiveData<>();
        Call<ImageUploadResponse> call = imageService.uploadSingle(file);

        call.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.d("response", "" + response.body());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                Log.e("fail", t.getLocalizedMessage());
            }
        });

        return liveData;
    }

}
