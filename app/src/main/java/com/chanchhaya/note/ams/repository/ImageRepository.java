package com.chanchhaya.note.ams.repository;

import android.net.Uri;

import androidx.lifecycle.MutableLiveData;

import com.chanchhaya.note.ams.api.response.ImageUploadResponse;

import java.io.File;

import okhttp3.MultipartBody;

public interface ImageRepository {

    MutableLiveData<ImageUploadResponse> uploadSingle(MultipartBody.Part file);

}
