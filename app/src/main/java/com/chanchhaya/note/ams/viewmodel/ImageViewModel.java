package com.chanchhaya.note.ams.viewmodel;

import android.net.Uri;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.chanchhaya.note.ams.api.response.ImageUploadResponse;
import com.chanchhaya.note.ams.repository.impl.ImageRepositoryImpl;

import java.io.File;

import okhttp3.MultipartBody;

public class ImageViewModel extends ViewModel {

    private ImageRepositoryImpl imageRepository;
    private MutableLiveData<ImageUploadResponse> liveData;

    public void init() {
        imageRepository = new ImageRepositoryImpl();
    }

    public MutableLiveData<ImageUploadResponse> uploadSingle(MultipartBody.Part file) {
        return imageRepository.uploadSingle(file);
    }

}
