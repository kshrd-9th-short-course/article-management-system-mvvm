package com.chanchhaya.note.ams.repository;

import androidx.lifecycle.MutableLiveData;

import com.chanchhaya.note.ams.api.request.CreateArticleRequest;
import com.chanchhaya.note.ams.api.response.BaseResponse;
import com.chanchhaya.note.ams.model.Article;
import com.chanchhaya.note.ams.model.Pagination;

import java.util.List;

public interface ArticleRepository {

    MutableLiveData<BaseResponse<List<Article>>> findAll(Pagination pagination);
    MutableLiveData<BaseResponse<Article>> createNew(CreateArticleRequest article);
    MutableLiveData<BaseResponse<Article>> deleteById(int id);
    MutableLiveData<BaseResponse<Article>> updateById(int id, CreateArticleRequest article);

}
